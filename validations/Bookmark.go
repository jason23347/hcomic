package validations

// Bookmark with information
type Bookmark struct {
	ComicID  string `validate:"required"`
	PageNum  uint   `validate:"required"`
	CoverURL string `validate:"required"`
	Title    string `validate:"max=32"`
}