package validations

import (
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"

	"github.com/go-playground/validator"
	en_translations "github.com/go-playground/validator/translations/en"
)

// Validate function
func Validate(model interface{}) map[string]string {
	validate := validator.New()

	enUS := en.New()
	uni := ut.New(enUS)
	trans, _ := uni.GetTranslator("zh")
	en_translations.RegisterDefaultTranslations(validate, trans)

	err := validate.Struct(model)
	if err == nil {
		return nil
	}

	return err.(validator.ValidationErrors).Translate(trans)
}
