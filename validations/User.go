package validations

// User contains user information.
type User struct {
	Name     string `validate:"required"`
	Password string `validate:"required,min=6"`
}
