package main

import (
	"flag"
	"html/template"
	"strconv"
	"time"

	"./migrations/orm"
	"./routes"
	"./routes/ajax"
	"./tools"

	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
	"github.com/kataras/iris/middleware/logger"
	"github.com/kataras/iris/middleware/methodoverride"
	"github.com/kataras/iris/middleware/recover"
	"github.com/kataras/iris/sessions"
)

func main() {
	serve := flag.Bool("serve", true, "start http server")
	sync := flag.Bool("sync", false, "sync the posts")
	migrate := flag.Bool("migrate", false, "migrate database")
	port := flag.Uint("port", 8080, "set http server port")
	flag.Parse()

	if *migrate {
		tools.RunMigrate()
	} else if *sync {
		tools.RunSync()
	} else if *serve {
		// ensure database contains necessary tables
		tools.RunMigrate()
		app := newApp()
		app.Run(iris.Addr(":" + strconv.FormatUint(uint64(*port), 10)))
	}
}

func ajaxRequireLogin(ctx iris.Context) hero.Result {
	sess := ctx.Values().Get("session").(*sessions.Session)
	_, ok := sess.Get("user").(*orm.User)
	if !ok {
		return hero.Response{
			Code: 403,
			Object: iris.Map{
				"errcode":    "1001",
				"errmessage": "login required",
			},
		}
	}
	ctx.Next()
	return nil
}

func newApp() *iris.Application {
	app := iris.New()

	app.Use(recover.New())
	app.Use(logger.New())

	// session
	sess := sessions.New(sessions.Config{
		Cookie:       "hcomic_session_id",
		Expires:      2 * time.Hour,
		AllowReclaim: true,
	})
	app.Use(func(ctx iris.Context) {
		ctx.Values().Set("session", sess.Start(ctx))
		ctx.Next()
	})

	// auth
	app.Use(func(ctx iris.Context) {
		sess := ctx.Values().Get("session").(*sessions.Session)
		_, ok := sess.Get("user").(*orm.User)

		var token string
		var db *gorm.DB
		var user orm.User
		var err error

		var isLogin bool = false
		if ok {
			isLogin = true
			goto res
		}

		token = ctx.GetCookie("hcomic_user")
		if token == "" {
			goto res
		}

		db = orm.Boot()
		defer db.Close()

		if err = db.Where(orm.User{APIToken: token}).
			First(&user).Error; err == nil {
			sess.Set("user", &user)
			isLogin = true
		}

	res:
		ctx.ViewData("IsLogin", isLogin)
		ctx.Next()
	})

	// views
	templates := iris.HTML("views", ".html")
	templates.AddFunc("toDate", func(t time.Time) string {
		return t.Format("2006-01-02 15:04")
	})
	templates.AddFunc("unescape", func(str string) interface{} {
		return template.HTML(str)
	})
	app.RegisterView(templates)

	// routes
	app.Get("/index", hero.Handler(routes.HomePageHandler))
	app.Get("/", func(ctx iris.Context) {
		ctx.Redirect("/index")
	})

	routes.LoginBind(app, "/", routes.UserBuilder{})
	routes.Restful(app, "/user", routes.UserBuilder{})
	routes.Restful(app, "/bookmark", routes.BookmarkBuilder{})
	app.PartyFunc("/user", func(r iris.Party) {
		r.Get("/favorite", routes.LoginCheck, hero.Handler(routes.ComicFavorite))
	})

	routes.Restful(app, "/comic", routes.ComicBuilder{})
	app.PartyFunc("/comic", func(r iris.Party) {
		r.Get("/{id:uint}/gallery", hero.Handler(routes.ComicGallery))
	})
	routes.Restful(app, "/post", routes.PostBuilder{})

	// about page
	app.Get("/about", func(ctx iris.Context) {
		ctx.Redirect("/post/1")
	})

	// ajax routes
	app.PartyFunc("/ajax", func(r iris.Party) {
		requireLogin := hero.Handler(ajaxRequireLogin)
		r.Get("/comic", hero.Handler(ajax.ComicIndex))
		r.Get("/comic/{id:uint}/favor", requireLogin, hero.Handler(ajax.ComicFavor))
	})

	// method override
	// header: X-HTTP-METHOD-OVERRIDE
	// form field: _method
	mo := methodoverride.New(methodoverride.SaveOriginalMethod("_originalMethod"))
	app.WrapRouter(mo)

	// resources
	// FIXME dumplicated directory for folders in public
	app.HandleDir("/", "public")
	app.HandleDir("/manhua", "public/manhua")
	app.HandleDir("/summary", "public/summary")
	app.HandleDir("/assets", "public/assets")
	app.HandleDir("/bootstrap", "public/bootstrap")

	// style sheets and js scripts
	app.HandleDir("/css", "public/css")
	app.HandleDir("/js", "public/js")

	return app
}
