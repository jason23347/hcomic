package routes

import (
	"../migrations/orm"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
)

// HomePageHandler function
func HomePageHandler(ctx iris.Context) hero.Result {
	db := orm.Boot()
	defer db.Close()

	var posts []orm.Post
	var comics []orm.Comic
	db.Order("date desc").Limit(2).Find(&posts)
	orm.Paginate(db.Order("created_at desc"), &comics, 1, 8)

	return hero.View{
		Layout: "shared/layout.html",
		Name:   "user/index.html",
		Data: iris.Map{
			"Title":  "H-COMIC | 不起眼的漫画浏览网站",
			"Posts":  posts,
			"Comics": comics,
		},
	}
}
