package ajax

import (
	"../../migrations/orm"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
	"github.com/kataras/iris/sessions"
)

// ComicIndex Service
func ComicIndex(ctx iris.Context) hero.Response {
	db := orm.Boot()
	defer db.Close()

	page := ctx.URLParamIntDefault("p", 1)
	size := ctx.URLParamIntDefault("s", 20)
	keyword := ctx.URLParamDefault("q", "")
	favorite := ctx.URLParamIntDefault("f", 0)

	if favorite > 0 {
		sess := ctx.Values().Get("session").(*sessions.Session)
		user, ok := sess.Get("user").(*orm.User)
		if !ok {
			return hero.Response{
				Code: 403,
				Object: iris.Map{
					"list":      nil,
					"totalSize": 0,
				},
			}
		}

		db = db.Joins("JOIN user_comics ON user_comics.comic_id = comics.id "+
			"AND user_comics.user_id = ?", user.ID)
	}

	db = db.Where("title LIKE ? OR title_en LIKE ?",
		"%"+keyword+"%", "%"+keyword+"%")
	var comics []orm.Comic
	orm.Paginate(db.Order("cast(folder as int) desc"),
		&comics, page, size)

	var num int
	db.Model(orm.Comic{}).Count(&num)

	return hero.Response{
		Object: iris.Map{
			"list":      comics,
			"totalSize": num,
		},
	}
}

// ComicFavor Service
func ComicFavor(ctx iris.Context, id uint) hero.Result {
	sess := ctx.Values().Get("session").(*sessions.Session)
	user, _ := sess.Get("user").(*orm.User)

	db := orm.Boot()
	defer db.Close()

	like := ctx.URLParamIntDefault("like", 1)

	var comic orm.Comic
	var err error
	db.Where("id = ?", id).First(&comic)
	if like > 0 {
		err = db.Model(&user).Association("FavoredComics").Append(comic).Error
	} else {
		err = db.Model(&user).Association("FavoredComics").Delete(comic).Error
	}

	errcode := 0
	errmessage := "success"

	if err != nil {
		errcode = 2001
		errmessage = err.Error()
	}

	return hero.Response{
		Object: iris.Map{
			"errcode":    errcode,
			"errmessage": errmessage,
		},
	}
}
