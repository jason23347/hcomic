package routes

import (
	"../migrations/orm"
	"../validations"

	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
	"github.com/kataras/iris/sessions"
)

// BookmarkBuilder struct
type BookmarkBuilder struct{}

// Index function
func (c BookmarkBuilder) Index(ctx iris.Context) hero.Result {
	sess := ctx.Values().Get("session").(*sessions.Session)
	user, _ := sess.Get("user").(*orm.User)

	db := orm.Boot()
	defer db.Close()

	var bookmarks []orm.Bookmark
	db.Where("user_id = ?", user.ID).Find(&bookmarks)

	return hero.View{
		Name:   "user/bookmark.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title":     "Bookmark Collections",
			"Bookmarks": bookmarks,
		},
	}
}

// Show function
func (c BookmarkBuilder) Show(ctx iris.Context, id uint) hero.Result { return NotFoundResponse(nil) }

// Create function
func (c BookmarkBuilder) Create(ctx iris.Context) hero.Result { return NotFoundResponse(nil) }

var invalidDataResponse = hero.Response{
	Code: 403,
	Object: iris.Map{
		"errcode": 1002,
		"message": "invalid post value",
	},
}

// Store function
func (c BookmarkBuilder) Store(ctx iris.Context) hero.Result {
	sess := ctx.Values().Get("session").(*sessions.Session)
	user, _ := sess.Get("user").(*orm.User)

	var bmValidator validations.Bookmark
	errInfo, err := validate(ctx, &bmValidator, validations.Validate)
	if err != nil {
		return IntervalServerError(err)
	}

	if errInfo != nil {
		return invalidDataResponse
	}

	db := orm.Boot()
	defer db.Close()
	err = db.Save(&orm.Bookmark{
		UserID:   user.ID,
		ComicID:  bmValidator.ComicID,
		PageNum:  bmValidator.PageNum,
		Title:    bmValidator.Title,
		CoverURL: bmValidator.CoverURL,
	}).Error
	if err != nil {
		return AjaxResponse(2001, err)
	}

	return hero.Response{
		Object: iris.Map{
			"errcode":    0,
			"errmessage": "success",
		},
	}
}

// Update function
func (c BookmarkBuilder) Update(ctx iris.Context, id uint) hero.Result {
	sess := ctx.Values().Get("session").(*sessions.Session)
	user := sess.Get("user").(*orm.User)

	db := orm.Boot()
	defer db.Close()

	var bookmark orm.Bookmark
	err := db.Where(iris.Map{
		"id":      id,
		"user_id": user.ID,
	}).First(&bookmark).Error
	if err != nil {
		return AjaxResponse(2001, err)
	}

	bookmark.Title = ctx.PostValueDefault("Title", "")
	err = db.Save(&bookmark).Error
	if err != nil {
		return AjaxResponse(2001, err)
	}

	return AjaxResponse(0, "success")
}

// Delete function
func (c BookmarkBuilder) Delete(ctx iris.Context, id uint) hero.Result {
	if id <= 0 {
		return invalidDataResponse
	}

	db := orm.Boot()
	defer db.Close()

	var bookmark orm.Bookmark
	db.Where("id = ?", id).First(&bookmark)
	if bookmark.ID <= 0 {
		return AjaxResponse(2001, "not exists")
	}
	err := db.Delete(&bookmark).Error
	if err != nil {
		return AjaxResponse(2001, err)
	}
	return AjaxResponse(0, "deleted")
}

// Edit function
func (c BookmarkBuilder) Edit(ctx iris.Context, id uint) hero.Result { return NotFoundResponse(nil) }
