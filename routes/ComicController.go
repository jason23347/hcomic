package routes

import (
	"../migrations/orm"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
)

// ComicBuilder is a restful controller
type ComicBuilder struct{}

// Index Service
// Url: /
func (c ComicBuilder) Index(ctx iris.Context) hero.Result {
	return hero.View{
		Name:   "comic/index.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title":     "Comic Index",
			"UseSearch": true,
			"Keyword":   ctx.URLParam("s"),
		},
	}
}

// Show Service
// Url: /{id}
func (c ComicBuilder) Show(ctx iris.Context, id uint) hero.Result {
	db := orm.Boot()
	defer db.Close()

	var comic orm.Comic
	db.Where("id = ?", id).Find(&comic)

	var liked int = 0
	user, ok := loginCheck(ctx)
	if ok {
		var num int
		db.Table("user_comics").
			Where("user_id = ? AND comic_id = ?", user.ID, comic.ID).
			Count(&num)
		if num > 0 {
			liked = 1
		}
	}

	return hero.View{
		Name:   "comic/show.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title": comic.Title,
			"Comic": comic,
			"Liked": liked,
		},
	}
}

// ComicGallery Service
// not binded to ComicBuilder
// TODO be more elegant
func ComicGallery(ctx iris.Context, id uint) hero.Result {
	db := orm.Boot()
	defer db.Close()

	var comic orm.Comic
	db.Where("folder = ?", id).Find(&comic)

	return hero.View{
		Name:   "comic/gallery.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title": comic.Title,
			"Comic": comic,
		},
	}
}

// ComicFavorite function
func ComicFavorite(ctx iris.Context) hero.Result {
	return hero.View{
		Name:   "comic/index.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title":     "Favorites",
			"UseSearch": false,
			"Favorite":  true,
		},
	}
}

// Create function
func (c ComicBuilder) Create(ctx iris.Context) hero.Result { return nil }

// Store function
func (c ComicBuilder) Store(ctx iris.Context) hero.Result { return nil }

// Edit function
func (c ComicBuilder) Edit(ctx iris.Context, id uint) hero.Result { return nil }

// Update function
func (c ComicBuilder) Update(ctx iris.Context, id uint) hero.Result { return nil }

// Delete function
func (c ComicBuilder) Delete(ctx iris.Context, id uint) hero.Result { return nil }
