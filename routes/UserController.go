package routes

import (
	"crypto/md5"
	"encoding/base64"
	"encoding/hex"
	"net/http"
	"strings"
	"time"

	"github.com/jinzhu/gorm"

	"../migrations/orm"
	"../validations"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
	"github.com/kataras/iris/sessions"
)

// UserBuilder is a restful controller
type UserBuilder struct {
	User  validations.User
	Model *orm.User
}

func (c UserBuilder) md5(str string) string {
	h := md5.New()
	h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}

// Register Service
// Method: POST
// Url: /register
func (c UserBuilder) Register(ctx iris.Context) hero.Result {
	var userV validations.User
	errInfo, err := validate(ctx, &userV, validations.Validate)

	if err != nil {
		return IntervalServerError(err)
	}

	// validate succceed
	if errInfo == nil {
		var user orm.User
		db := orm.Boot()
		defer db.Close()

		err = db.Where("name = ?", userV.Name).First(&user).Error
		if err == nil { // already exists
			errInfo = make(map[string]string)
			errInfo["User.Name"] = "username is already in use"
		} else if gorm.IsRecordNotFoundError(err) {
			user = orm.User{
				Name:     userV.Name,
				Password: c.md5(userV.Password),
			}
			err = db.Save(&user).Error
			if err != nil {
				return IntervalServerError(err)
			}
			return hero.View{
				Name: "shared/redirect.html",
				Data: iris.Map{
					"Info": "Register succeed",
					"URL":  "/login",
				},
			}
		} else {
			return AjaxResponse(2001, err)
		}
	}

	info := make(map[string]string)
	isInvalid := make(map[string]string)
	for index, err := range errInfo {
		slices := strings.Split(index, ".") // User.Name
		in := slices[len(slices)-1]         // last section
		info[in] = err
		isInvalid[in] = "is-invalid"
	}

	return hero.View{
		Name:   "login/register.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title":     "User Register Page",
			"FormTitle": "Register",
			"IsInvalid": isInvalid,
			"ErrInfo":   info,
			"Old":       userV,
		},
	}
}

// RegisterView Service
// Method: GET
// Url: /register
func (c UserBuilder) RegisterView(ctx iris.Context) hero.Result {
	return hero.View{
		Name:   "login/register.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title":     "User Register Page",
			"FormTitle": "Register",
		},
	}
}

// Login Service
// Method: POST
// Url: /login
func (c UserBuilder) Login(ctx iris.Context) hero.Result {
	sess := ctx.Values().Get("session").(*sessions.Session)

	var userV validations.User
	errInfo, err := validate(ctx, &userV, validations.Validate)

	if err != nil {
		return IntervalServerError(err)
	}

	// validate succeed
	if errInfo == nil {
		db := orm.Boot()
		defer db.Close()

		var user orm.User
		err = db.Where("name = ? AND password = ?",
			userV.Name, c.md5(userV.Password)).
			First(&user).Error
		// db validate succeed
		if err == nil {
			now := time.Now()
			token := base64.StdEncoding.EncodeToString([]byte(user.Name + now.Format(time.ANSIC)))
			user.APIToken = token
			db.Save(&user)

			// set session
			sess.Set("user", &user)
			ctx.SetCookie(&http.Cookie{
				Name:    "hcomic_user",
				Value:   token,
				Expires: now.AddDate(0, 3, 0),
			})
			return hero.View{
				Name: "shared/redirect.html",
				Data: iris.Map{
					"Info": "Login succeed",
					"URL":  "/",
				},
			}
		} else if gorm.IsRecordNotFoundError(err) {
			errInfo = make(map[string]string)
			errInfo["User.Name"] = "Incorrect username or password"
		}
	}

	info := make(map[string]string)
	isInvalid := make(map[string]string)
	for index, err := range errInfo {
		slices := strings.Split(index, ".") // User.Name
		in := slices[len(slices)-1]         // last section
		info[in] = err
		isInvalid[in] = "is-invalid"
	}

	return hero.View{
		Code:   403,
		Name:   "login/register.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title":     "User Register Page",
			"FormTitle": "Login",
			"IsInvalid": isInvalid,
			"ErrInfo":   info,
			"Old":       userV,
		},
	}
}

// LoginView Service
// Method: GET
// Url: /login
func (c UserBuilder) LoginView(ctx iris.Context) hero.Result {
	return hero.View{
		Name:   "login/register.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title":     "User Login Page",
			"FormTitle": "Login",
		},
	}
}

// Logout Service
// Method: POST
// Url: /logout
func (c UserBuilder) Logout(ctx iris.Context) hero.Result {
	return NotFoundResponse(nil)
}

// Index Service
// Url: /
func (c UserBuilder) Index(ctx iris.Context) hero.Result {
	return NotFoundResponse(nil)
}

// Show Service
// Url: /{id}
func (c UserBuilder) Show(ctx iris.Context, id uint) hero.Result {
	return NotFoundResponse(nil)
}

// Create function
func (c UserBuilder) Create(ctx iris.Context) hero.Result { return NotFoundResponse(nil) }

// Store function
func (c UserBuilder) Store(ctx iris.Context) hero.Result { return NotFoundResponse(nil) }

// Edit function
func (c UserBuilder) Edit(ctx iris.Context, id uint) hero.Result { return NotFoundResponse(nil) }

// Update function
func (c UserBuilder) Update(ctx iris.Context, id uint) hero.Result { return NotFoundResponse(nil) }

// Delete function
func (c UserBuilder) Delete(ctx iris.Context, id uint) hero.Result { return NotFoundResponse(nil) }
