package routes

import (
	"../migrations/orm"
	"github.com/jinzhu/gorm"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
	"github.com/kataras/iris/sessions"
)

// RestfulBuilder Struct
// a restful interface
type RestfulBuilder interface {
	Index(iris.Context) hero.Result
	Show(iris.Context, uint) hero.Result
	Create(iris.Context) hero.Result
	Store(iris.Context) hero.Result
	Edit(iris.Context, uint) hero.Result
	Update(iris.Context, uint) hero.Result
	Delete(iris.Context, uint) hero.Result
}

// Restful func
// binding routes and RestfulBuilder
func Restful(app *iris.Application, relativePath string, partyBuilder RestfulBuilder) {
	app.PartyFunc(relativePath, func(r iris.Party) {
		r.Get("/", hero.Handler(partyBuilder.Index))
		r.Get("/{id:uint}", hero.Handler(partyBuilder.Show))
		r.Get("/create", hero.Handler(partyBuilder.Create))
		r.Post("/", hero.Handler(partyBuilder.Store))
		r.Get("/{id:uint}/edit", hero.Handler(partyBuilder.Edit))
		r.Put("/{id:uint}", hero.Handler(partyBuilder.Update))
		r.Patch("/{id:uint}", hero.Handler(partyBuilder.Update))
		r.Delete("/{id:uint}", hero.Handler(partyBuilder.Delete))
	})
}

// LoginBuilder with user actions
type LoginBuilder interface {
	Register(iris.Context) hero.Result
	RegisterView(iris.Context) hero.Result

	Login(iris.Context) hero.Result
	LoginView(iris.Context) hero.Result

	Logout(iris.Context) hero.Result
}

// LoginCheck middleware
func LoginCheck(ctx iris.Context) {
	sess := ctx.Values().Get("session").(*sessions.Session)
	_, ok := sess.Get("user").(*orm.User)

	if !ok {
		ctx.Redirect("/login")
	}
	ctx.Next()
}

// LoginRedirect middlware
func LoginRedirect(ctx iris.Context) {
	sess := ctx.Values().Get("session").(*sessions.Session)
	_, ok := sess.Get("user").(*orm.User)

	if ok {
		ctx.Redirect("/")
	}
	ctx.Next()
}

// LoginBind function
func LoginBind(app *iris.Application, relativePath string, loginBuilder LoginBuilder) {
	app.PartyFunc(relativePath, func(r iris.Party) {
		r.Get("/register", LoginRedirect, hero.Handler(loginBuilder.RegisterView))
		r.Post("/register", LoginRedirect, hero.Handler(loginBuilder.Register))
		r.Get("/login", LoginRedirect, hero.Handler(loginBuilder.LoginView))
		r.Post("/login", LoginRedirect, hero.Handler(loginBuilder.Login))

		r.Post("/logout", LoginCheck, hero.Handler(loginBuilder.Logout))
	})
}

func dbErrorResponse(err error) hero.Response {
	if gorm.IsRecordNotFoundError(err) {
		return NotFoundResponse(err)
	}
	// TODO add log
	return IntervalServerError(err)
}

// NotFoundResponse returns 404
func NotFoundResponse(err error) hero.Response {
	return hero.Response{
		Code: 404,
		Err:  err,
	}
}

// IntervalServerError returns 500
func IntervalServerError(err error) hero.Response {
	return hero.Response{
		Code: 500,
		Err:  err,
	}
}

// AjaxResponse return ajax response
func AjaxResponse(code int, message interface{}) hero.Response {
	return hero.Response{
		Object: iris.Map{
			"errcode":    code,
			"errmessage": message,
		},
	}
}

func loginCheck(ctx iris.Context) (*orm.User, bool) {
	sess := ctx.Values().Get("session").(*sessions.Session)
	user, ok := sess.Get("user").(*orm.User)
	return user, ok
}

func validate(ctx iris.Context, user interface{}, validation func(interface{}) map[string]string) (errInfo map[string]string, err error) {
	if err = ctx.ReadForm(user); err != nil {
		return nil, err
	}

	if errInfo = validation(user); errInfo != nil {
		return errInfo, nil
	}

	return nil, nil
}