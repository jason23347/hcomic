package routes

import (
	"../migrations/orm"
	"github.com/kataras/iris"
	"github.com/kataras/iris/hero"
)

// PostBuilder is a restful builder
type PostBuilder struct{}

// Index Service
// Method: GET
// Url: /
func (c PostBuilder) Index(ctx iris.Context) hero.Result {
	db := orm.Boot()
	defer db.Close()

	var posts []orm.Post
	db.Order("date desc").Find(&posts)

	return hero.View{
		Name:   "post/index.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Title": "Post Index Page",
			"List": iris.Map{
				"Posts": posts,
			},
		},
	}
}

// Show Service
// Method: GET
// Param: id uint
// Url: /{postid}
func (c PostBuilder) Show(ctx iris.Context, id uint) hero.Result {
	db := orm.Boot()
	defer db.Close()

	var post orm.Post
	var tags []orm.Tag

	if err := db.Where("id = ?", id).Find(&post).Error; err != nil {
		return dbErrorResponse(err)
	}
	post.ReadNum++
	db.Save(post)

	db.Model(&post).Related(&tags, "tags")
	return hero.View{
		Name:   "post/show.html",
		Layout: "shared/layout.html",
		Data: iris.Map{
			"Post": post,
			"Tags": tags,
		},
	}
}

// Create Service
// Method: GET
// Url: /create
func (c PostBuilder) Create(ctx iris.Context) hero.Result { return nil }

// Store Service
// Method: POST
// Url: /
func (c PostBuilder) Store(ctx iris.Context) hero.Result { return nil }

// Edit Service
// Method: GET
// Param: id uint
// Url: /{postid}/edit
func (c PostBuilder) Edit(ctx iris.Context, postid uint) hero.Result { return nil }

// Update Service
// Method: PUT
// Param: id uint
// Url: /{postid}
func (c PostBuilder) Update(ctx iris.Context, postid uint) hero.Result { return nil }

// Delete Service
// Method: DELETE
// Param: id uint
// Url: /{postid}
func (c PostBuilder) Delete(ctx iris.Context, postid uint) hero.Result { return nil }
