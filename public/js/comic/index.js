$(document).ready(function () {
    function setTop(ele, value) {
        ele.style.top = value
    }

    list = document.getElementById("index-list")
    origin_height = 56

    var content = document.getElementById("navbarSupportedContent")
    var search_height = content.children[0].children.length * 56
    var itemNum = content.children[1].children.length
    const height = search_height + 40 * itemNum
    $("#navbarSupportedContent").
        on('show.bs.collapse', function (e) {
            var nav = document.getElementById("navbar-first")
            setTop(list, nav.offsetHeight + height)
        }).
        on('hide.bs.collapse', function (e) {
            var list = document.getElementById("index-list")
            var nav = document.getElementById("navbar-first")
            setTop(list, origin_height)
        })
})

var app = new Vue({
    el: "#index-list",
    data: {
        keyword: "",
        list: [],
        mescroll: {},
        page: {
            num: 0,
            size: 20,
        },
    },
    mounted: function () {
        this.keyword = document.
            getElementsByTagName("meta").
            keyword.content
        var favorite = document.
            getElementsByTagName("meta").
            favorite.content

        url = "/ajax/comic?"
        if (favorite === "true")
            url += "f=1&"
        var self = this
        this.mescroll = new MeScroll("index-list", {
            down: {
                auto: false,
                callback: function () {
                    self.list = []
                    self.$nextTick(() => {
                        self.mescroll.resetUpScroll(true)
                    })
                }
            },
            up: {
                page: self.page,
                empty: {
                    warpId: "index-list",
                    tip: "无记录～",
                },
                htmlNodata: '<p class="upwarp-nodata col-12 float-left">-- END --</p>',
                callback: function (page) {
                    var num = page.num
                    var size = page.size
                    axios.get(url + "p=" + num + "&s=" + size + "&q=" + self.keyword).
                        then((res) => {
                            var curPageData = res.data
                            var totalSize = curPageData.totalSize
                            self.list = self.list.concat(curPageData.list)
                            self.$nextTick(() => {
                                self.mescroll.endBySize(curPageData.list.length, totalSize)
                            })
                        }).
                        catch((err) => {
                            if (err.response.status == 403)
                                self.mescroll.endUpScroll(true)
                        })
                },
            },
        })
    },
    methods: {},
})
