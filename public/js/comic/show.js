const folder = document.
    getElementsByTagName("meta").
    comic.content

var app = new Vue({
    el: "#info",
    data: {
        info: {},
        icon: "favorite_border",
        liked: 0,
        lock: false,
    },
    mounted: function () {
        this.liked = parseInt(document.getElementsByTagName("meta").
            liked.content) || 0
        if (this.liked) this.icon = "favorite"
        
        var self = this
        axios.get("/summary/" + folder + ".json").
            then((res) => {
                self.info = res.data
            })
    },
    methods: {
        favorHandler() {
            if (this.lock) return
            this.lock = true

            var self = this
            axios.get("/ajax" + document.location.pathname + "/favor?like=" + (!self.liked + 0)).
                then((res) => {
                    console.log(res)
                    if (!res.data.errcode) {
                        self.liked = !self.liked
                    }
                    if (self.liked)
                        self.icon = "favorite"
                    else
                        self.icon = "favorite_border"
                }).
                then((res) => {
                    self.lock = false
                }).
                catch((err) => {
                    console.log(err.response)
                })
        }
    },
})