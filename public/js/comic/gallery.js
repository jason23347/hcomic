const folder = document.
    getElementsByTagName("meta").
    comic.content

var app = new Vue({
    el: "#picture",
    data: function () {
        return {
            popup: false,
            title: "",
            prompt: "",
            prompt_list: [
                "黑夜给了我黑色的眼睛，我却用它来寻找光明。",
                "那天一位智者在此处筑起高台，点亮了一个信标。",
                "愿黑夜中的明星，为我指点方向。",
            ],
            pictures: [{ "name": "../../assets/loading.png" }],
            total: 1,
            current: 1,
            cachedImages: [],
            cacheNum: 3,
            fab: {},
        }
    },
    created: function () {
        window.addEventListener("popstate", this.JumpPage)
    },
    mounted: function () {
        this.fab = new mdui.Fab("#fab")
        this.fab.hide()

        this.initPrompt()

        var self = this
        axios.get("/summary/" + folder + ".json").
            then((res) => {
                self.pictures = res.data.Pictures
                self.total = res.data.Pictures.length
                self.JumpPage()
                for (let i = 0; i < self.total; i++) {
                    self.cachedImages[i] = new Image()
                }
            })
    },
    methods: {
        Anchor: function (num) {
            window.location.hash = "#" + num
        },
        JumpPage() {
            var num = parseInt(window.location.hash.substring(1))
            if (num >= 1 && num <= this.total)
                this.current = num;
            else
                this.current = 1;
            document.getElementsByClassName("mdui-progress-determinate")[0].style.width
                = Math.floor((this.current / this.total) * 100) + "%";
        },
        ClickHandler: function (event) {
            x = event.clientX
            var middle = document.body.clientWidth / 2
            var width = document.body.clientWidth * 0.1
            if (Math.abs(middle - x) < 80) {
                console.log(Math.abs(middle - x))
                this.fab.show()
            } else if (x < middle) {
                this.fab.hide()
                // previous page
                this.current = (this.current + this.total - 2) % this.total + 1;
            } else {
                this.fab.hide()
                // next page
                this.current = this.current % this.total + 1;
                for (let i = 0; i < this.cacheNum; i++) {
                    var tmp = (this.current + i + 1) % this.total
                    this.CacheImage(tmp, this.pictures[tmp].name)
                }
            }
            if (this.current === 1)
                this.$Message.info("这是第一页");
            else if (this.current === this.total)
                this.$Message.info("这是最后一页");
            this.Anchor(this.current)
        },
        async CacheImage(index, url) {
            this.cachedImages[index].src = "/manhua/" + folder + '/' + url
        },
        homeHandler(id) {
            window.location.href = "/comic/" + id
        },
        bookmarkHandler() {
            this.fab.close()
            this.popup = true
        },
        favoriteHandler(id) {
            this.fab.close()
            var self = this
            axios.get("/ajax/comic/" + id + "/favor").
                then((res) => {
                    if (!res.data.errcode) {
                        self.$Message.success("favored")
                    } else {
                        self.$Message.error("failed")
                    }
                }).
                catch((err) => {
                    console.log(err.response)
                    if (err.response.status == 403)
                        self.$Message.error(err.response.data.errmessage)
                    else
                        self.$Message.error("error")
                })
        },
        initPrompt() {
            this.prompt = this.prompt_list[Math.floor(
                Math.random() * this.prompt_list.length
            )]
        },
        bookmark() {
            var formData = new FormData()
            formData.append("Title", this.title)
            formData.append("ComicID", folder)
            formData.append("PageNum", this.current)
            formData.append("CoverURL",
                "/manhua/" + folder + "/" + this.pictures[this.current].name)

            var self = this
            axios.post("/bookmark", formData).
                then((res) => {
                    self.title = ""
                    self.popup = false
                }).
                then((res) => {
                    self.$Message.success("Success");
                }).
                catch((err) => {
                    self.$Message.error("Failed!!");
                })
        },
    },
})