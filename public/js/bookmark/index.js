var app = new Vue({
    el: "#popup-windows",
    data: {
        lock: false,
        popedit: false,
        popdelete: false,
        title: "",
        id: 0,
    },
    methods: {
        edit(id, title) {
            if (this.lock == true) return
            this.lock = false

            this.id = id
            this.title = title
            this.popedit = true
        },
        editHandler() {
            var formData = new FormData()
            formData.append("Title", this.title)

            var self = this
            axios.post("/bookmark/" + this.id, formData, {
                headers: {
                    "X-HTTP-METHOD-OVERRIDE": "PUT",
                }
            }).
                then((res) => {
                    self.$Message.success("修改成功，请刷新页面查看")
                }).
                catch((err) => {
                    console.log(err.response)
                    self.$Message.error("error")
                }).
                finally(() => {
                    this.popedit = false

                    self.title = ""
                    self.id = 0

                    self.lock = false
                })
        },
        remove(id) {
            if (this.lock == true) return
            this.lock = false

            this.id = id
            this.popdelete = true
        },
        removeHandler() {
            var self = this
            axios.post("/bookmark/" + self.id, "", {
                headers: {
                    "X-HTTP-METHOD-OVERRIDE": "DELETE",
                }
            }).
                then((res) => {
                    if (res.data.errcode == 0)
                        self.$Message.success("success")
                    else
                        self.$Message.error("error")
                }).
                catch((err) => {
                    console.log(err.response)
                    self.$Message.error("error")
                }).
                finally((res) => {
                    this.popedit = false
                    this.lock = false
                })
        },
    },
})