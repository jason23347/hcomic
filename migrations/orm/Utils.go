package orm

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite" // database connection
)

// Boot function
// Start a database connection
func Boot() *gorm.DB {
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("failed to connect database")
	}
	return db
}

// Paginate function
// ... well, paginate
func Paginate(db *gorm.DB, out interface{}, page, size int) {
	db.Limit(size).
		Offset(size * (page - 1)).
		Find(out)
}
