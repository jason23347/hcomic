package orm

import (
	"time"

	"github.com/jinzhu/gorm"
)

// Post struct
type Post struct {
	gorm.Model

	Title    string
	Date     time.Time
	CoverURL string
	ReadNum  uint
	Content  string `gorm:"type:text"`
	HashInfo string

	Tags []Tag `gorm:"many2many:post_tags"`
}
