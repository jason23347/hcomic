package orm

import (
	"github.com/jinzhu/gorm"
)

// Tag struct
type Tag struct{
	gorm.Model

	Name string
}