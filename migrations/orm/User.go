package orm

import "github.com/jinzhu/gorm"

// User struct
type User struct {
	gorm.Model

	Name     string
	Password string
	APIToken string

	Posts         []Post     `gorm:"foreignkey:UserID"`
	Bookmarks     []Bookmark `gorm:"foreignkey:UserID"`
	FavoredComics []Comic    `gorm:"many2many:user_comics;"`
}
