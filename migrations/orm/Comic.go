package orm

import (
	"github.com/jinzhu/gorm"
)

// Comic struct
type Comic struct {
	gorm.Model

	Folder  string
	Title   string
	TitleEn string

	CoverURL string
}
