package orm

import (
	"github.com/jinzhu/gorm"
)

// Bookmark struct
type Bookmark struct {
	gorm.Model

	UserID   uint   `gorm:"NOT NULL"`
	ComicID  string `gorm:"NOT NULL"`
	PageNum  uint   `gorm:"NOT NULL"`
	CoverURL string `gorm:"NOT NULL"`
	Title    string
}
