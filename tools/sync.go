package tools

import (
	"bufio"
	"io/ioutil"
	"os"
	"os/exec"
	"sort"
	"strings"
	"time"

	"../migrations/orm"
)

// RunSync function
func RunSync() {
	syncPosts()
	syncDatabase()
}

func syncDatabase() {
	db := orm.Boot()
	defer db.Close()

	// FIXME dumplicated code with RunSync()
	db.AutoMigrate(orm.Comic{})
	db.AutoMigrate(orm.Post{})
	db.AutoMigrate(orm.Tag{})
	db.AutoMigrate(orm.User{})
	db.AutoMigrate(orm.Bookmark{})
}

const postDir = "storage/posts/"

type post struct {
	Title      string
	Date       time.Time
	Categories []string
	CoverURL   string
	HashInfo   string
	Content    string
}

type sortByDate [0x0fff]post

func (a sortByDate) Len() int           { return len(a) }
func (a sortByDate) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a sortByDate) Less(i, j int) bool { return a[i].Date.After(a[j].Date) }

func syncPosts() {
	dir, err := ioutil.ReadDir(postDir)
	if err != nil {
		panic(err)
	}

	var posts sortByDate

	var num uint = 0
	for _, fileInfo := range dir {
		if fileInfo.IsDir() || fileInfo.Name() == ".gitkeep" {
			continue
		}
		md2html(postDir, fileInfo.Name(), &posts[num])
		num++
	}

	sort.Stable(posts)
	db := orm.Boot()
	defer db.Close()

	db.AutoMigrate(orm.Tag{})
	db.AutoMigrate(orm.Post{})

	for i := uint(0); i < num; i++ {
		// update post
		var post orm.Post
		db.Where("hash_info = ?", posts[i].HashInfo).
			FirstOrCreate(&post)

		post.Title = posts[i].Title
		post.Date = posts[i].Date
		post.CoverURL = posts[i].CoverURL
		post.Content = posts[i].Content
		post.HashInfo = posts[i].HashInfo
		db.Save(&post)

		// reset tags
		db.Model(&post).
			Association("Tags").
			Clear()
		for _, tagName := range posts[i].Categories {
			var tag orm.Tag
			db.Where(orm.Tag{
				Name: tagName,
			}).FirstOrCreate(&tag)
			db.Model(&post).
				Association("Tags").
				Append(tag)
		}
	}
}

func md2html(fileDir, fileName string, post *post) {
	file, err := os.Open(fileDir + fileName)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	rd := bufio.NewReader(file)

	var line string
	var slices []string
	line, _ = rd.ReadString('\n') // ---

	line, _ = rd.ReadString('\n') // title
	slices = strings.Split(line, ": ")
	post.Title = strings.Trim(slices[1], " \n")

	line, _ = rd.ReadString('\n') // date
	slices = strings.Split(line, ": ")
	post.Date, _ = time.Parse("2006-01-02 15:04:05", strings.Trim(slices[1], " \n"))

	line, _ = rd.ReadString('\n') // categories
	slices = strings.Split(line, ": ")
	line = strings.Trim(slices[1], "\n")
	post.Categories = strings.Split(line, " ")

	line, _ = rd.ReadString('\n') // cover
	slices = strings.Split(line, ": ")
	post.CoverURL = strings.Trim(slices[1], "\n")

	line, _ = rd.ReadString('\n') // hash
	slices = strings.Split(line, ": ")
	post.HashInfo = strings.Trim(slices[1], "\n")

	line, _ = rd.ReadString('\n') // ---

	htmlFile := strings.TrimSuffix(fileName, ".md") + ".html"

	cmd := exec.Command("pandoc", fileDir+fileName,
		"-o", "public/posts/"+htmlFile)
	cmd.Run()

	data, err := ioutil.ReadFile("public/posts/" + htmlFile)
	if err != nil {
		panic(err)
	}
	post.Content = string(data)
}
