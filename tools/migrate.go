package tools

import (
	"../migrations/orm"

	_ "github.com/jinzhu/gorm/dialects/sqlite" // database
)

// RunMigrate function
func RunMigrate() {
	db := orm.Boot()

	// Migrate the schema
	db.AutoMigrate(&orm.Comic{})
	db.AutoMigrate(&orm.Post{})
	db.AutoMigrate(&orm.Tag{})
	db.AutoMigrate(&orm.User{})
}
